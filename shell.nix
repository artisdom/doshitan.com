{ pkgs ? (import <nixpkgs> {})
, nodePackages ? pkgs.nodePackages
, haskellPackages ? pkgs.haskell.packages.ghc801
, haskellDevTools ? (if pkgs ? myHaskellDevTools then pkgs.myHaskellDevTools else (p : []))
}:

let
  modifiedHaskellPackages = haskellPackages.override {
    overrides = self: super: {
      doshitan-site = self.callPackage ./. { uglify-js = nodePackages.uglify-js; };
      hakyll = pkgs.haskell.lib.dontCheck super.hakyll;
    };
  };

  package = with modifiedHaskellPackages;
    pkgs.haskell.lib.addBuildTools
      doshitan-site
      ((haskellDevTools modifiedHaskellPackages) ++ [cabal-install pkgs.awscli pkgs.gnumake]);
in
  package.env
