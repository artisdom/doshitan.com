module Site.Fields where

import           Data.Monoid           (mconcat)
import           Data.String           (fromString)
import           Hakyll
import           System.FilePath.Posix (splitFileName)

pageCtx :: Context String
pageCtx = mconcat
  [ modificationTimeField "date-modified" humanTimeFmt
  , modificationTimeField "date-modified-machine" machineTimeFmt
  , field "toc-content" toc
  , urlField' "url"
  , defaultContext
  ]
  where
    toc item = loadBody ((itemIdentifier item) { identifierVersion = Just "toc" })

postCtx :: Tags -> Context String
postCtx tags = mconcat
  [ dateField "date" humanTimeFmt
  , dateField "date-machine" machineTimeFmt
  , tagsField "tags" tags
  , pageCtx
  ]

humanTimeFmt, machineTimeFmt :: String
humanTimeFmt = "%B %e, %Y"
machineTimeFmt = "%Y-%m-%d"

-- Use URL of normal version of file
-- See http://blaenkdenum.com/posts/post-feed-in-hakyll/
-- Also remove index.html from links, i.e., url is /page/ not /page/index.html
urlField' :: String -> Context a
urlField' key = field key $
  fmap (maybe "" $ removeIndexStr . toUrl) . getRoute . setVersion Nothing . itemIdentifier
  where removeIndexStr url = case splitFileName url of
          (dir, "index.html") -> dir
          _                   -> url

data ExtraLink = ExtraLink
  {
    extraLinkUrl  :: String
  , extraLinkName :: String
  }

-- | Obtain extra links from a page in the default way: parse them from the
-- @extra-links@ metadata field.
getExtraLinks :: MonadMetadata m => Identifier -> m [ExtraLink]
getExtraLinks identifier = do
  metadata <- getMetadata identifier
  return $ maybe [] (map (uncurry ExtraLink . read)) $ lookupStringList "extra-links" metadata

extraLinkToItem :: ExtraLink -> Item ExtraLink
extraLinkToItem extra = Item (fromString $ extraLinkUrl extra) extra

extraLinkCtx :: Context ExtraLink
extraLinkCtx = mconcat
  [
    field "url" $ return . extraLinkUrl . itemBody
  , field "name" $ return . extraLinkName . itemBody
  ]
