module Site.Config where

import Hakyll

hakyllConf :: Configuration
hakyllConf = defaultConfiguration
  { deployCommand = "rsync -avz -e ssh ./_site/ doshitan@doshitan.com:/var/www/doshitan.com/" }

feedConf :: String -> FeedConfiguration
feedConf title = FeedConfiguration
  { feedTitle = "doshitan.com: " ++ title
  , feedDescription = "Personal site of Tanner Doshier"
  , feedAuthorName = "Tanner Doshier"
  , feedAuthorEmail = "doshitan@gmail.com"
  , feedRoot = "http://doshitan.com"
  }

-- Pages which need special processing and are therefore excluded from the
-- general processing for other pages
specialPages :: [Identifier]
specialPages = wrapElems "pages/" "" ["index.html"]

-- Which Bootstrap javascript plugins to include
-- Choose from: alert button carousel collapse dropdown modal popover scrollspy tab tooltip transition typeahead
bootstrapPlugins :: [Identifier]
bootstrapPlugins = wrapElems "assets/js/bootstrap/" ".js" [ "button"
                                                          , "collapse"
                                                          , "popover"
                                                          , "tooltip"
                                                          , "transition"
                                                          ]

-- Prepend and/or append content to each element in a list of strings
wrapElems :: String         -- What to prepend (goes before)
          -> String         -- What to append  (goes after)
          -> [String]       -- The list to wrap
          -> [Identifier]   -- The wrapped strings as identifiers
wrapElems b a = map (fromFilePath . \x -> b ++ x ++ a)
