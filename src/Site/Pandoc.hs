module Site.Pandoc where

import Hakyll
import Text.Pandoc         (Block (Header))
import Text.Pandoc.Options (HTMLMathMethod (PlainMath), WriterOptions,
                            writerHTMLMathMethod, writerHtml5,
                            writerNumberSections, writerSectionDivs,
                            writerStandalone, writerTOCDepth,
                            writerTableOfContents, writerTemplate)
import Text.Pandoc.Walk    (walk)

writerOptions :: WriterOptions
writerOptions = defaultHakyllWriterOptions { writerHtml5 = True
                                           , writerSectionDivs = True
                                           , writerTableOfContents = True
                                           }

mPandocCompiler :: Compiler (Item String)
mPandocCompiler = pandocCompilerWithTransform defaultHakyllReaderOptions writerOptions (walk bumpHeaders)

feedPandocCompiler :: Compiler (Item String)
feedPandocCompiler = pandocCompilerWith defaultHakyllReaderOptions writerOptions'
  where writerOptions' = defaultHakyllWriterOptions { writerHtml5 = True
                                                    , writerHTMLMathMethod = PlainMath
                                                    }

tocPandocCompiler :: Compiler (Item String)
tocPandocCompiler = pandocCompilerWith defaultHakyllReaderOptions writerOptions'
  where writerOptions' = writerOptions { writerNumberSections = True
                                       , writerTemplate = "$toc$"
                                       , writerStandalone = True
                                       , writerTOCDepth = 2
                                       }

-- | Map every @h1@ to an @h2@, @h2@ to @h3@, etc. like 'demoteHeaders', but
-- as a pandoc transformer
bumpHeaders :: Block -> Block
bumpHeaders (Header lvl attrs inlines) = Header (lvl + 1) attrs inlines
bumpHeaders x = x
