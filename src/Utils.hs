module Utils where

import Data.Char (isAlphaNum, isSpace, toLower)
import Data.List (intercalate)

safeName :: String -> String
safeName = lowerWithDashes . filter (\c -> isAlphaNum c || isSpace c)

lowerWithDashes :: String -> String
lowerWithDashes = intercalate "-" . words . toLowerStr

toLowerStr :: String -> String
toLowerStr = map toLower
