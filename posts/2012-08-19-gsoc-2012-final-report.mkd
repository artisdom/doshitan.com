---
title: "GSoC 2012: Final Report"
meta-description: Where things stand at the end of GSoC
tags: GNOME, GSoC
---

The end has come. Work for Google Summer of Code ends tomorrow and it's been
a fun ride. I've learned a lot and it's always nice to get paid to work on
something you love. I don't think any of my SoC work will make it into 3.6
(since UI freeze also happens to be tomorrow), which is unfortunate. But, it
also means that the whole search experience in 3.8 should be improved, instead
of incremental updates.

You can find pretty much all the work I've done spread across a few bugs:

* [Bug 640966 - Favorite applications should have priority when searching](https://bugzilla.gnome.org/show_bug.cgi?id=640966)
    * This is near-ready, just need a decision what exactly should get priority.
* [Bug 681797 - Switch to a List for Search Results UI](https://bugzilla.gnome.org/show_bug.cgi?id=681797)
    * Also near-ready, just a matter of how to properly handle keyboard
      navigation
* [Bug 682050 - Hide overview elements while searching](https://bugzilla.gnome.org/show_bug.cgi?id=682050)
    * This one's a bit more tricky. The current patches on the bug work, but
      break the behavior of hiding most of the workspace thumbnails when they
      aren't being utilized (when < 2 workspaces are in use). The best fix is
      to refactor the overview into a St.BoxLayout or something similar that
      is a lot more automatic/flexible in the layout. I've started
      experimenting with this and have a basic working version that should be
      ready for 3.8.

So a number of things are, for practical purposes, done, but not widely tested
enough to go into a stable product, particularly at the last minute. I'm
comfortable with this, I would rather 3.8 have a solid, cohesive experience
than a 3.6 with some piecemeal patches.

Other work I hope to do in the coming months includes sorting remote providers
in the right order, providing the user a way to pick which search providers
they want enabled, and indicate why a result was returned (highlight terms).
With my fall semester also starting tomorrow though, my time for hacking on
the shell will be greatly reduced. Even so, I think I can still get some
things done and at the very least make sure all the work I did this summer is
in proper shape.

Thanks to all the people who helped me get, and get through, GSoC; in
particular Marina, Rui, and Jasper.
